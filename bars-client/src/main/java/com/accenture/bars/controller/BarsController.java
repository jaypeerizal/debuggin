package com.accenture.bars.controller;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.bars.circuitbreaker.BarsHystrix;
import com.accenture.bars.domain.Customer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


/**
 * BarsController Class
 *
 * @author christian.p.c.mirano
 * @since 17.10.18
 */

/**
 * BarsController - controller of the Bars Web App
 *
 */

@EnableCircuitBreaker
@Controller
public class BarsController {

	@Autowired
	BarsHystrix barsHystrix;

	static String role;

	@RequestMapping(value = "/bars")
	public String roleFromAccount(
			@RequestParam(value = "rights", required = false) String role,
			Model model){
		String page = "";

		if("admin".equals(role)) {
			page = "barsadmin";
		} else if ("user".equals(role)) {
			page = "barsnonadmin";
		}
		this.role = role;
		model.addAttribute("rights", role);

		return page;
	}

	@RequestMapping(value = "/")
	public String goToBarsHome() {
		String page = "";
		if("admin".equals(role)) {
			page = "barsadmin";
		} else if ("user".equals(role)) {
			page = "barsnonadmin";
		}
		return page;
	}

	@RequestMapping(value = "/process")
	public ModelAndView processRequest(@RequestParam("files")File file) throws JSONException {

		return barsHystrix.execute(file, role);
	}

	@RequestMapping(value = "/active")
	public String processActive(Model model) throws JSONException {

		return "barsactive";
	}


	@RequestMapping(value = "/delete")
	public String processDelete(@RequestParam("customerId")String customerId, Model model) throws JSONException {
		JSONObject json = new JSONObject();
		json.put("customerId", customerId);
		String page = "";
		Client client = new Client();

		//Change this
		WebResource wr = client.resource("http://localhost:2000/bars/deleteAccount");
		ClientResponse response = wr.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json.toString());
		String restStr = response.getEntity(String.class);
		System.out.println(restStr);
		System.out.println(response.getStatus());

		if(response.getStatus() == 200){
			 page = "";

			if("admin".equals(role)) {
				page = "barsactive";
			} else if ("user".equals(role)) {
				page = "barsnonadmin";
			}
			this.role = role;
			model.addAttribute("rights", role);
			return page;


		}
		//return with error in thymeleaf  ERRORC
		ModelAndView mv = new ModelAndView();
		mv.addObject("ERRORC", "ERROR Deleting Customer");
		return page;

	}



	@RequestMapping(value = "/create")
	public String processCreate(@RequestParam("firstName")String firstName,
										@RequestParam("lastName")String lastName,
										@RequestParam("address")String address,
										@RequestParam("status")String status, Model model) throws JSONException {
		String page = "";

		JSONObject json = new JSONObject();
		json.put("firstName", firstName);
		json.put("lastName", lastName);
		json.put("address",address);
		json.put("status",status);
		json.put("last_edited", role);

		Client client = new Client();

		//Change this for load balancing later
		WebResource wr = client.resource("http://localhost:2000/bars/createAccount");
		ClientResponse response = wr.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json.toString());
		String restStr = response.getEntity(String.class);
		System.out.println(restStr);
		System.out.println(response.getStatus());

		if(response.getStatus() == 200){
			 page = "";

			if("admin".equals(role)) {
				page = "barsactive";
			} else if ("user".equals(role)) {
				page = "barsnonadmin";
			}
//			this.role = role;
			model.addAttribute("rights", role);
			return page;

		}

		ModelAndView mv = new ModelAndView();
		mv.addObject("ERRORC", "ERROR Adding Customer");
		return page;
	}


	@RequestMapping(value = "/update")
	public ModelAndView processUpdate(@RequestParam("customerId") String customerId) throws JSONException {

		ModelAndView mv = new ModelAndView();
		String url = "http://localhost:2000/bars/getaccountsE/" + customerId;
		Client client = new Client();
		WebResource wr = client.resource(url);
		ClientResponse cr = wr.type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

		Customer customer = new Customer(cr.getEntity(String.class));
		customer.addModelAndView(mv);
		mv.setViewName("barsupdate");

		return mv;


	}

	//updatecustomer
	@RequestMapping(value = "/updatecustomer")
	public String processSubmittedUpdate(@RequestParam("customerId") String customerId,
										 @RequestParam("firstName") String firstName,
										 @RequestParam("lastName") String lastName,
										 @RequestParam("address") String address,
										 @RequestParam("status") String status,
										 Model model) throws JSONException {
		//Method Here
		//last edited = bc.role
		String page = "";

		JSONObject json = new JSONObject();
		json.put("customerId", customerId);
		json.put("firstName", firstName);
		json.put("lastName", lastName);
		json.put("address",address);
		json.put("status",status);
		json.put("lastEdited", role);
		System.out.println("ROROROROROLE: "+role);


		Client client = new Client();

		//Change this for load balancing later
		WebResource wr = client.resource("http://localhost:2000/bars/updateAccount");
		ClientResponse response = wr.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json.toString());
		String restStr = response.getEntity(String.class);
		System.out.println(restStr);
		System.out.println(response.getStatus());

		if(response.getStatus() == 200){
			 page = "";

			if("admin".equals(role)) {
				page = "barsactive";
			} else if ("user".equals(role)) {
				page = "barsnonadmin";
			}
//			this.role = role;
			model.addAttribute("rights", role);
			return page;

		}

		return page;
	}









}