package com.accenture.bars.domain;

import java.sql.Timestamp;
//import java.util.Set;





import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonFormat;
//import com.fasterxml.jackson.annotation.JsonManagedReference;

public class Customer {

	private Long customerId;
	private String firstName;
	private String lastName;
	private String address;
	private String status;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm")
	private Timestamp dateCreated;
	private String lastEdited;


	public Customer() {
	}

	public void addModelAndView(ModelAndView mv) {
		mv.addObject("customerId", this.customerId);
		mv.addObject("firstName", this.firstName);
		mv.addObject("lastName", this.lastName);
		mv.addObject("address", this.address);
		mv.addObject("status", this.status);
	}

	public Customer(String jsonObject) throws JSONException {
		JSONObject accountJson = new JSONObject(jsonObject);
		this.customerId = (Long) accountJson.getLong("customerId");
		this.firstName = (String) accountJson.getString("firstName");
		this.lastName = (String) accountJson.getString("lastName");
		this.address = (String) accountJson.getString("address");
		this.status = (String) accountJson.getString("status");

	}

	public Customer(String firstName, String lastName, String address,
			String status, Timestamp dateCreated, String lastEdited) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.status = status;
		this.dateCreated = dateCreated;
		this.lastEdited = lastEdited;

	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getLastEdited() {
		return lastEdited;
	}

	public void setLastEdited(String lastEdited) {
		this.lastEdited = lastEdited;
	}


}
