package com.accenture.bars.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.accenture.bars.domain.Account;
import com.accenture.bars.domain.Customer;
import com.accenture.bars.domain.Record;
import com.accenture.bars.domain.Request;
import com.accenture.bars.exception.BarsException;
import com.accenture.bars.factory.InputFileFactory;
import com.accenture.bars.file.IInputFile;
import com.accenture.bars.file.IOutputFile;
import com.accenture.bars.file.XMLOutputFileImpl;


import com.accenture.bars.repository.AccountRepository;
import com.accenture.bars.repository.CustomerRepository;
import com.accenture.bars.repository.RequestRepository;


@Path("/bars")
public class BarsService {

	private static Logger log = LoggerFactory
			.getLogger(BarsService.class);


	@Autowired
	RequestRepository requestRepository;
	IInputFile inputFile;
	IOutputFile outputFile;


	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	//http://localhost:2000/bars/getaccountsE/customerId
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getaccountsE/{customerId}")
	public Response getAccount(@PathParam("customerId") Long customerId) throws JSONException {

		Customer customer = customerRepository.findOne(customerId);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("customerId", customerId);
		jsonObject.put("firstName", customer.getFirstName());
		jsonObject.put("lastName", customer.getLastName());
		jsonObject.put("address", customer.getAddress());
		jsonObject.put("status", customer.getStatus());
		jsonObject.put("dateCreated", customer.getDateCreated());

		return Response.status(200).entity(jsonObject.toString()).type(MediaType.APPLICATION_JSON).build();

	}



	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getcustomer")
	public List<Customer> getCustomer() {

		//change to
		//List<Customer> accounts = customerRepository.findAll();
		List<Customer> customers = customerRepository.find("Y");
	    //System.out.println("May laman ba : "+customerRepository.findActive().isEmpty());
		return customers;
		  //return accounts;
	}



	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getaccount")
	public List<Account> getAccount() {

		//change to
		//List<Customer> accounts = customerRepository.findAll();
		List<Account> accounts = accountRepository.find("Y");
	    //System.out.println("May laman ba : "+customerRepository.findActive().isEmpty());
		return accounts;
		  //return accounts;
	}










	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/createAccount")
	public Response creatAccounts(String customer) throws JSONException {
		//add method here
		JSONObject json = new JSONObject(customer);


	    Customer cust = new Customer();
	    cust.setFirstName(json.getString("firstName"));
	    cust.setLastName(json.getString("lastName"));
	    cust.setAddress(json.getString("address"));
	    cust.setStatus(json.getString("status"));
	    cust.setDateCreated(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));
	    cust.setLastEdited(json.getString("last_edited"));

	    customerRepository.saveAndFlush(cust);

	    JSONObject jsonres = new JSONObject();
	    jsonres.put("INFO", "Successfully Created");
	    return Response.status(200).entity(jsonres.toString()).type(MediaType.APPLICATION_JSON).build();


	}

	//http://localhost:2000/bars/updaetAccount"
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateAccount")
	public Response updatAccounts(Customer customer) throws JSONException {
		//add method here
		Customer custom = customer;
		custom.setDateCreated(new java.sql.Timestamp(Calendar.getInstance().getTime().getTime()));

	    customerRepository.save(customer);

	    JSONObject jsonres = new JSONObject();
	    jsonres.put("INFO", "Successfully Updated");
	    return Response.status(200).entity(jsonres.toString()).type(MediaType.APPLICATION_JSON).build();


	}




	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteAccount")
	public Response deleteAccounts(String customer) throws JSONException {
		//add method here
		JSONObject json = new JSONObject(customer);

		String id = json.getString("customerId");
		System.out.println("ANg lamn ng ID ay : "+id);
		Customer custToDelete = new Customer();

		custToDelete = customerRepository.findOne(Long.decode(id));
		customerRepository.delete(custToDelete);

	    JSONObject jsonres = new JSONObject();
	    jsonres.put("INFO", "Successfully Deleteeeedddd");
	    return Response.status(200).entity(jsonres.toString()).type(MediaType.APPLICATION_JSON).build();
	}


	// http://localhost:1998/bars/execute?file=FILE
	@GET
	@Path("/execute")
	@Produces(MediaType.APPLICATION_JSON)
	public Response execute(@QueryParam("file") String file)
			throws JSONException {
		log.info("Access /execute");
		log.info("ang file ay: "+file);

		JSONObject json = new JSONObject();

		inputFile = InputFileFactory.getInstance().getInputFile(new File(file));

		if ("".equals(file)) {
			json.put("ERROR", BarsException.PATH_DOES_NOT_EXIST);
			return Response.status(405).type(MediaType.APPLICATION_JSON)
					.entity(json.toString()).build();
		} else if (null == inputFile) {
			json.put("ERROR", BarsException.NO_SUPPORT_FILE);
			return Response.status(405).type(MediaType.APPLICATION_JSON)
					.entity(json.toString()).build();
		}

		inputFile.setFile(new File(file));

		try {
			List<Request> requests = inputFile.readFile();
			if (requests.isEmpty()) {
				json.put("ERROR", BarsException.NO_RECORDS_TO_READ);
				return Response.status(405).type(MediaType.APPLICATION_JSON)
						.entity(json.toString()).build();
			}

			for (Request request : requests) {
				requestRepository.save(request);
			}

			List<Record> records = fileProcessorRetrieveRecords();

			if (records.isEmpty()) {
				requestRepository.deleteAll();
				json.put("ERROR", BarsException.NO_RECORDS_TO_WRITE);
				return Response.status(405).type(MediaType.APPLICATION_JSON)
						.entity(json.toString()).build();
			}

			writeOutput(records);

			return Response.status(200).build();

		} catch (BarsException e) {
			json.put("ERROR", e.getMessage());
			return Response.status(405).type(MediaType.APPLICATION_JSON)
					.entity(json.toString()).build();
		}

	}

	// http://localhost:1998/bars/getrecords
	@GET
	@Path("/getrecords")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Record> getRecords() {
		log.info("Access /getrecords");
		List<Record> records = fileProcessorRetrieveRecords();
		requestRepository.deleteAll();
		return records;
	}

	public List<Record> fileProcessorRetrieveRecords() {
		log.info("Access /fileProcessorRetrieveRecords");
		List<Object[]> objectRecords = requestRepository.findRecords();
		List<Record> records = new ArrayList<>();
		for (Object[] object : objectRecords) {
			records.add(new Record((int) object[0],
					(java.sql.Date) object[1], (java.sql.Date) object[2],
					(String) object[3], (String) object[4],
					(double) object[5]));
		}
		return records;
	}

	public void writeOutput(List<Record> records) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"MMddyyyy_HHmmss");
		String date = dateFormat.format(new Date());

		outputFile = new XMLOutputFileImpl();
		outputFile.setFile(new File("C:/BARS/Report/BARS_Report-" + date
				+ ".xml"));
		outputFile.writeFile(records);


	}

}
