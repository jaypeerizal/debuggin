package com.accenture.bars.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.accenture.bars.domain.Customer;
//import com.accenture.bars.domain.Request;


public interface CustomerRepository extends JpaRepository<Customer, Long>{

//	@Query(value = "Select customerId, firstName, lastName, address, status, dateCreated, lastEdited from Customer")
//	List<Customer> findActive(String status);

	 @Query("SELECT c FROM Customer c WHERE LOWER(c.status) = LOWER(:status)")
	    public List<Customer> find(@Param("status") String status);

	//Customer findByUsername(String username);

}
