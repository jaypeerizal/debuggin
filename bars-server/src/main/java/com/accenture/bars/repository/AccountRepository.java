package com.accenture.bars.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.accenture.bars.domain.Account;



public interface AccountRepository extends JpaRepository<Account, Long>{

	 @Query("SELECT a FROM Account a WHERE LOWER(a.isActive) = LOWER(:isActive)")
	    public List<Account> find(@Param("isActive") String stat);

	/* @Query("SELECT c FROM Customer c WHERE LOWER(c.status) = LOWER(:status)")
	    public List<Customer> find(@Param("status") String status);

	    */


}
